# Ethereum Towers Bot

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/etbot/main?style=plastic)](https://bitbucket.org/idomdavis/etbot/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/etbot?style=plastic)](https://bitbucket.org/idomdavis/etbot/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/etbot?style=plastic)](https://bitbucket.org/idomdavis/etbot/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/etbot)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

A Bot for Ethereum Towers
