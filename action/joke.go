package action

import (
	"fmt"

	"bitbucket.org/idomdavis/discordance"
	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/etbot/colour"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Joke action.
type Joke struct {
	Base
}

type joke struct {
	ID     int    `json:"id"`
	Author string `json:"author"`
	Joke   string `json:"joke"`
}

var defaultJoke = joke{
	Joke: "A bot tries to load its jokes from the Database, the query fails.",
}

// Definition for the joke command.
func (j *Joke) Definition() *command.Definition {
	return &command.Definition{
		Name:        j.Command,
		Description: j.Description,
		Handler:     j.Handle,
	}
}

// Handle the joke command.
func (j *Joke) Handle(event command.Event) error {
	if !j.event(event) {
		return nil
	}

	event.Respond(&discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Embeds: []*discordgo.MessageEmbed{j.Get(event.Session)},
		},
	})

	return nil
}

// Handler listens for the joke command.
func (j *Joke) Handler(s *discordgo.Session, in *discordgo.Message) {
	if !j.message(in) {
		return
	}

	_, err := s.ChannelMessageSendComplex(in.ChannelID,
		&discordgo.MessageSend{Embed: j.Get(s)})

	if err != nil {
		logrus.WithError(err).Error("Failed to send joke")
	}
}

// Get a joke as a MessageEmbed.
func (j *Joke) Get(s *discordgo.Session) *discordgo.MessageEmbed {
	var item joke

	n, err := j.Random("et-jokes", &item)

	if err != nil {
		item = defaultJoke

		logrus.WithError(err).Error("Failed to get random joke")
	}

	user := &discordance.User{
		ID:     item.Author,
		Name:   "Someone Random",
		Avatar: "https://tentcity.info/icons/et.png",
	}

	if err = user.Populate(s); err != nil {
		logrus.WithError(err).WithField("user", item.Author).Error(
			"Failed to get joke author details")
	}

	user.Name = fmt.Sprintf("A joke by %s", user.Name)

	return &discordgo.MessageEmbed{
		Author:      user.Author(),
		Description: item.Joke,
		Color:       colour.Pink,
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("Joke #%d of %d", item.ID+1, n),
		},
	}
}
