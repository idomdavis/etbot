package action

import (
	"time"

	"bitbucket.org/idomdavis/etbot/datetime"
	"bitbucket.org/idomdavis/etbot/db"
	"bitbucket.org/idomdavis/etbot/embed"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Announcer used to display Announcements.
type Announcer struct {
	Refresh bool      `json:"refresh"`
	Bot     string    `json:"bot"`
	Channel string    `json:"channel"`
	Last    time.Time `json:"last"`
	Next    time.Time `json:"next"`

	Authors    *embed.Authors     `json:"-"`
	Session    *discordgo.Session `json:"-"`
	Connection *db.Connection     `json:"-"`

	data Announcements
}

// Run the Announcer.
func (a *Announcer) Run() {
	if a.Bot == "" {
		logrus.Warn("No bot set")
		return
	}

	if a.Authors == nil || a.Session == nil || a.Connection == nil {
		logrus.Error("Invalid Announcer")
		return
	}

	go a.tick()
}

func (a *Announcer) tick() {
	ticker := time.NewTicker(time.Minute)

	for range ticker.C {
		a.refresh()

		if a.Next.After(time.Now()) || a.Channel == "" || len(a.data) == 0 {
			continue
		}

		announcement := a.data.Current().Next()
		author := a.Authors.Get(announcement.Author)
		msg := embed.Build(datetime.Convert(announcement.Message), author)

		if _, err := a.Session.ChannelMessageSendComplex(a.Channel, msg); err != nil {
			logrus.WithError(err).Error("Failed to send announcements")
		}

		a.Last = time.Now()
		a.Next = next(a.Last, a.data.Current().Count())

		announcement.Update(a.Connection)
		a.update()
	}
}

func (a *Announcer) refresh() {
	k := map[string]interface{}{"bot": a.Bot}

	if err := a.Connection.Get("et-announcer", k, a); err != nil {
		logrus.WithError(err).WithField("bot", a.Bot).Error(
			"Failed to load announcer config")

		return
	}

	if a.Refresh || len(a.data) == 0 {
		a.data.Load(a.Connection)
		a.Next = next(a.Last, a.data.Current().Count())
		a.Refresh = false

		a.update()
	}
}

func (a *Announcer) update() {
	params := []interface{}{a.Last, a.Next, a.Refresh, a.Bot}

	if err := a.Connection.Execute(updateQuery, params, nil); err != nil {
		logrus.WithError(err).WithField("bot", a.Bot).Error("Failed to update announcer")
	}
}

func next(last time.Time, items int) time.Time {
	const window = time.Hour * 4

	if last.IsZero() {
		last = time.Now()
	}

	if items == 0 {
		items = 1
	}

	return last.Add(window / time.Duration(items))
}

const updateQuery = `update "et-announcer" 
set "last" = ?
set "next" = ? 
set "refresh" = ? 
where "bot" = ?`
