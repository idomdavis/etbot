package action

import (
	"fmt"

	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/etbot/datetime"
	"bitbucket.org/idomdavis/etbot/embed"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Preview commands.
type Preview struct {
	Base
	Authors *embed.Authors
}

// Definition for the room preview command.
func (p *Preview) Definition() *command.Definition {
	return &command.Definition{
		Name:        p.Command,
		Description: p.Description,
		Handler:     p.Handle,

		Options: []*discordgo.ApplicationCommandOption{{
			Type:        discordgo.ApplicationCommandOptionInteger,
			Name:        "id",
			Description: "ID of the announcement to preview",
			Required:    true,
		}},
	}
}

// Handle the room-service command.
func (p *Preview) Handle(event command.Event) error {
	if !p.event(event) {
		return nil
	}

	id := event.Interaction.ApplicationCommandData().Options[0].IntValue()

	announcement := p.Get(int(id))
	author := p.Authors.Get(announcement.Author)
	preview := embed.Build(datetime.Convert(announcement.Message), author)
	msg := fmt.Sprintf("Annoucement %d preview:", id)

	if !announcement.Start.IsZero() {
		msg = fmt.Sprintf("%s\n> Starts: <t:%d>", msg, announcement.Start.Unix())
	} else {
		msg = fmt.Sprintf("%s\n> No Start Set", msg)
	}

	if !announcement.End.IsZero() {
		msg = fmt.Sprintf("%s\n> Ends: <t:%d>", msg, announcement.End.Unix())
	} else {
		msg = fmt.Sprintf("%s\n> No End Set", msg)
	}

	event.Respond(&discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: msg,
			Embeds:  preview.Embeds,
			AllowedMentions: &discordgo.MessageAllowedMentions{
				Parse: []discordgo.AllowedMentionType{discordgo.AllowedMentionTypeUsers},
			},
		},
	})

	return nil
}

// Get the given announcement.
func (p *Preview) Get(id int) Announcement {
	var a Announcement

	k := map[string]interface{}{"id": id}

	if err := p.Connection.Get("et-announcements", k, &a); err != nil {
		logrus.WithError(err).Error("Failed to get announcement")
	}

	return a
}
