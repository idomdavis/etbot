package action

import (
	"fmt"

	"bitbucket.org/idomdavis/discordance/command"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Menu action.
type Menu struct {
	Base
}

type order struct {
	Item string `json:"item"`
}

// Definition for the room service command.
func (m *Menu) Definition() *command.Definition {
	return &command.Definition{
		Name:        m.Command,
		Description: m.Description,
		Handler:     m.Handle,
	}
}

// Handle the room-service command.
func (m *Menu) Handle(event command.Event) error {
	if !m.event(event) {
		return nil
	}

	event.Respond(&discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: m.Get(event.User()),
			AllowedMentions: &discordgo.MessageAllowedMentions{
				Parse: []discordgo.AllowedMentionType{discordgo.AllowedMentionTypeUsers},
			},
		},
	})

	return nil
}

// Handler listens for the menu command.
func (m *Menu) Handler(s *discordgo.Session, in *discordgo.Message) {
	if !m.message(in) {
		return
	}

	_, err := s.ChannelMessageSendComplex(in.ChannelID,
		&discordgo.MessageSend{Content: m.Get(in.Author.ID)})

	if err != nil {
		logrus.WithError(err).Errorf("Failed to send %s", m.Command)
	}
}

// Get item from the menu for the given user.
func (m *Menu) Get(user string) string {
	var o order

	if _, err := m.Random("et-menu", &o); err != nil {
		logrus.WithError(err).Error("Failed to get random menu item")

		o.Item = "order"
	}

	return fmt.Sprintf("Your %s, <@%s>", o.Item, user)
}
