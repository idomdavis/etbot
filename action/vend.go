package action

import (
	"fmt"

	"bitbucket.org/idomdavis/discordance/command"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Vend action.
type Vend struct {
	Base
}

type product struct {
	Name string `json:"name"`
}

// Definition for the room service command.
func (m *Vend) Definition() *command.Definition {
	return &command.Definition{
		Name:        m.Command,
		Description: m.Description,
		Handler:     m.Handle,
	}
}

// Handle the room-service command.
func (m *Vend) Handle(event command.Event) error {
	if !m.event(event) {
		return nil
	}

	event.Respond(&discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: m.Get(event.User()),
			AllowedMentions: &discordgo.MessageAllowedMentions{
				Parse: []discordgo.AllowedMentionType{discordgo.AllowedMentionTypeUsers},
			},
		},
	})

	return nil
}

// Handler listens for the vend command.
func (m *Vend) Handler(s *discordgo.Session, in *discordgo.Message) {
	if !m.message(in) {
		return
	}

	_, err := s.ChannelMessageSendComplex(in.ChannelID,
		&discordgo.MessageSend{Content: m.Get(in.Author.ID)})

	if err != nil {
		logrus.WithError(err).Errorf("Failed to send %s", m.Command)
	}
}

// Get item from the vending machine for the given user.
func (m *Vend) Get(user string) string {
	var o product

	if _, err := m.Random("et-vending", &o); err != nil {
		logrus.WithError(err).Error("Failed to get random vending machine item")

		return "The machine is out of order!"
	}

	return fmt.Sprintf("The machine whirs, dispensing a %s for <@%s>", o.Name, user)
}
