package action

import (
	"time"

	"bitbucket.org/idomdavis/etbot/db"
	"github.com/sirupsen/logrus"
)

// Announcement to make.
type Announcement struct {
	ID      int       `json:"id"`
	Author  string    `json:"author"`
	Message string    `json:"message"`
	Start   time.Time `json:"start"`
	End     time.Time `json:"end"`
	Last    time.Time `json:"last"`
}

// Empty returns true if an Announcement is empty.
func (a *Announcement) Empty() bool {
	return a.Message == ""
}

// Update the last time the Announcement was made.
func (a *Announcement) Update(connection *db.Connection) {
	const query = `update "et-announcements" set "last" = ? where "id" = ?`

	a.Last = time.Now()
	params := []interface{}{a.Last, a.ID}

	if err := connection.Execute(query, params, nil); err != nil {
		logrus.WithError(err).WithField("id", a.ID).Error("Failed to update announcement")
	}
}
