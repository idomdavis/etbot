package action

import (
	"errors"
	"fmt"

	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/etbot/db"
	"bitbucket.org/idomdavis/etbot/rng"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Base action.
type Base struct {
	Command     string
	Description string

	Connection *db.Connection
}

// ErrTableEmpty is returned if a Random call is made on an empty table.
var ErrTableEmpty = errors.New("table is empty")

// Random item from a table with a numeric id column.
func (b *Base) Random(table string, result interface{}) (int, error) {
	size, err := b.Connection.Size(table)

	if err != nil {
		return 0, fmt.Errorf("failed to get table size: %w", err)
	} else if size == 0 {
		return 0, ErrTableEmpty
	}

	n := rng.Int(size)

	return size, b.Connection.Get(table, map[string]interface{}{"id": n}, result)
}

func (b *Base) channel(channel string) bool {
	var r struct {
		Channels []string `json:"channels"`
	}

	k := map[string]interface{}{"action": b.Command}

	if err := b.Connection.Get("et-channels", k, &r); err != nil {
		logrus.WithError(err).WithField("action", b.Command).Error("Failed to check action")
	}

	if len(r.Channels) == 0 {
		return true
	}

	for _, e := range r.Channels {
		if e == channel {
			return true
		}
	}

	return false
}

func (b *Base) event(event command.Event) bool {
	ok := b.channel(event.Interaction.ChannelID)

	if !ok {
		event.Respond(&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Flags:   command.Ephemeral,
				Content: fmt.Sprintf("You can't use `/%s` here", b.Command),
			},
		})
	}

	return ok
}

func (b *Base) message(in *discordgo.Message) bool {
	if in != nil && in.Content != "/"+b.Command {
		return false
	}

	return b.channel(in.ChannelID)
}
