package action

import (
	"fmt"
	"strings"

	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/etbot/audit"
	"github.com/bwmarrin/discordgo"
)

// Audit action.
type Audit struct {
	Base
}

// Named data which can have its named returned.
type Named interface {
	Names() []string
}

// Definition for the audit command.
func (a *Audit) Definition() *command.Definition {
	return &command.Definition{
		Name:        a.Command,
		Description: a.Description,
		Handler:     a.Handle,
	}
}

// Handle the audit command.
func (a *Audit) Handle(event command.Event) error {
	if !a.event(event) {
		return nil
	}

	id := event.Interaction.GuildID
	inspection := audit.Inspection{Roles: audit.RolePermissions}

	if err := inspection.Check(event.Session, id); err != nil {
		event.Respond(&discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Flags:   command.Ephemeral,
				Content: "Error running audit",
			},
		})

		return fmt.Errorf("failed to run audit: %w", err)
	}

	event.Respond(&discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: "Audit results",
			Embeds: []*discordgo.MessageEmbed{
				report("Admin Access", inspection.Admin),
				report("Role Permissions", inspection.Base),
				report("Channel Allow Overrides", inspection.Allow),
				report("Channel Deny Overrides", inspection.Deny),
			},
		},
	})

	return nil
}

func report(title string, data Named) *discordgo.MessageEmbed {
	const (
		length = 1500
		green  = 33872
		red    = 12066067

		separator = ", "
	)

	names := data.Names()

	if len(names) == 0 {
		return &discordgo.MessageEmbed{
			Title:       title,
			Description: "Audit passed!",
			Color:       green,
		}
	}

	description := "Please check: " + strings.Join(names, separator)

	if len(description) > length {
		description = description[0:length]

		i := strings.LastIndex(description, separator)

		description = description[0:i] + " ... "
	}

	return &discordgo.MessageEmbed{
		Title:       title,
		Description: description,
		Color:       red,
	}
}
