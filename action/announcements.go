package action

import (
	"sort"
	"time"

	"bitbucket.org/idomdavis/etbot/db"
	"github.com/sirupsen/logrus"
)

// Announcements to make.
type Announcements []*Announcement

// Current announcements, that is announcements that have a start time before
// now and an end time after now.
func (a *Announcements) Current() *Announcements {
	var current Announcements

	now := time.Now()

	for _, announcement := range *a {
		switch {
		case announcement.Start.After(now):
		// not started yet
		case announcement.End.IsZero():
			fallthrough
		case announcement.End.After(now):
			current = append(current, announcement)
		}
	}

	return &current
}

// Next announcement to make. This is determined by least recently announced
// item. If neither item has been announced then the item with the earliest
// start time is next.
func (a *Announcements) Next() *Announcement {
	announcements := *a

	if len(announcements) == 0 {
		return &Announcement{}
	}

	sort.Slice(announcements, func(i, j int) bool {
		if announcements[i].Last.IsZero() && announcements[j].Last.IsZero() {
			return announcements[i].Start.Before(announcements[j].Start)
		}

		return announcements[i].Last.Before(announcements[j].Last)
	})

	return announcements[0]
}

// Count of announcements.
func (a *Announcements) Count() int {
	return len(*a)
}

// Load the announcements.
func (a *Announcements) Load(connection *db.Connection) {
	const query = `select * from "et-announcements"`

	if err := connection.Execute(query, nil, a); err != nil {
		logrus.WithError(err).Error("Failed to load announcements")
	}
}
