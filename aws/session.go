package aws

import (
	"bitbucket.org/idomdavis/etbot/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

// Session used to connect to AWS.
func Session(settings config.AWS) (client.ConfigProvider, error) {
	c := credentials.NewStaticCredentials(settings.AccessKey, settings.SecretKey, "")

	return session.NewSession(&aws.Config{
		Credentials: c,
		Region:      &settings.Region,
	})
}
