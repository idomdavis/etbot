package db

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// Connection to DynamoDB.
type Connection struct {
	*dynamodb.DynamoDB
}

// New DynamoDB connection.
func New(session client.ConfigProvider) *Connection {
	return &Connection{DynamoDB: dynamodb.New(session)}
}

// Put an item into a table.
func (c *Connection) Put(table string, i interface{}) error {
	m, err := dynamodbattribute.MarshalMap(i)

	if err != nil {
		return fmt.Errorf("failed to marshal item for %s: %w", table, err)
	}

	_, err = c.PutItem(&dynamodb.PutItemInput{
		Item:      m,
		TableName: aws.String(table),
	})

	if err != nil {
		return fmt.Errorf("failed to store item in %s: %w", table, err)
	}

	return nil
}

// Get an item from a table.
func (c *Connection) Get(table string, keys map[string]interface{}, result interface{}) error {
	k, err := dynamodbattribute.MarshalMap(keys)

	if err != nil {
		return fmt.Errorf("invalid keys: %w", err)
	}

	input := &dynamodb.GetItemInput{TableName: aws.String(table), Key: k}

	if r, err := c.GetItem(input); err != nil {
		return fmt.Errorf("failed to retrieve item from %s: %w", table, err)
	} else if r.Item == nil {
		return nil
	} else if err = dynamodbattribute.UnmarshalMap(r.Item, &result); err != nil {
		return fmt.Errorf("failed to unmarshal item from %s: %w", table, err)
	}

	return nil
}

// Execute a PartiQL query.
func (c *Connection) Execute(stmt string, params []interface{}, results interface{}) error {
	input := &dynamodb.ExecuteStatementInput{Statement: &stmt}

	if len(params) > 0 {
		p, err := dynamodbattribute.MarshalList(params)

		if err != nil {
			return fmt.Errorf("invalid parameters: %w", err)
		}

		input.Parameters = p
	}

	if o, err := c.ExecuteStatement(input); err != nil {
		return fmt.Errorf("failed to execute statement: %w", err)
	} else if results == nil {
		return nil
	} else if err = dynamodbattribute.UnmarshalListOfMaps(o.Items, results); err != nil {
		return fmt.Errorf("failed to unmarshal results: %w", err)
	}

	return nil
}

// Size of the given table.
func (c *Connection) Size(table string) (int, error) {
	var results []struct {
		ID int `json:"id"`
	}

	query := fmt.Sprintf("select %q from %q", "id", table)

	if err := c.Execute(query, nil, &results); err != nil {
		return 0, fmt.Errorf("failed to describe table %s: %w", table, err)
	}

	return len(results), nil
}
