package main

import (
	"encoding/json"
	"os"

	"bitbucket.org/idomdavis/etbot/action"
	"bitbucket.org/idomdavis/etbot/aws"
	"bitbucket.org/idomdavis/etbot/config"
	"bitbucket.org/idomdavis/etbot/db"
	"github.com/sirupsen/logrus"
)

const path = "/Volumes/Development/src/bitbucket.org/idomdavis/etbot/"

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		PadLevelText:           true,
	})

	logrus.WithFields(logrus.Fields{
		"Build Version":   config.BuildVersion,
		"Build Hash":      config.BuildHash,
		"Build Timestamp": config.BuildTimestamp,
	}).Info("Build Info")

	os.Args = []string{os.Args[0], "-c", path + "config.json"}

	settings, err := config.Configure()

	switch {
	case err != nil:
		logrus.WithError(err).Fatal("Failed to configure migration tool")
	case settings.Underlying.Help:
		settings.Underlying.Usage(os.Stdout)
		os.Exit(0)
	}

	session, err := aws.Session(settings.AWS)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to get AWS session")
	}

	connection := db.New(session)
	update(connection)
}

func update(connection *db.Connection) {
	var data action.Announcements

	if b, err := os.ReadFile(path + "announcements.json"); err != nil {
		logrus.WithError(err).Fatal("Failed to get read announcements")
	} else if err = json.Unmarshal(b, &data); err != nil {
		logrus.WithError(err).Fatal("Failed to get marshal announcements")
	}

	for i, e := range data {
		e.ID = i
		if err := connection.Put("et-announcements", e); err != nil {
			logrus.WithError(err).Error("Failed to get write announcement")
		}
	}
}
