package datetime

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// Match a discord timestamp tag.
var timestampTag = regexp.MustCompile(`<t:(.+?)(?::\w)?>`)

const unknownTime = "[Time TBC]"

// Convert an input string with ISO timestamps into one that uses Unix
// timestamps.
func Convert(s string) string {
	for _, match := range timestampTag.FindAllStringSubmatch(s, -1) {
		if t, err := time.Parse(time.RFC3339, match[1]); err == nil {
			r := strings.Replace(match[0], match[1], fmt.Sprint(t.Unix()), 1)

			s = strings.Replace(s, match[0], r, 1)
		} else if _, err = strconv.ParseInt(match[1], 10, 64); err != nil {
			s = strings.Replace(s, match[0], unknownTime, 1)
		}
	}

	return s
}
