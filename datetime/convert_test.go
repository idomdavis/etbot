package datetime_test

import (
	"fmt"

	"bitbucket.org/idomdavis/etbot/datetime"
)

func ExampleConvert() {
	fmt.Println(datetime.Convert(`A sample set of timestamps to convert:
      Event A <t:1234567890:R>
      Event B <t:2021-12-20T13:14:04+00:00:D>
      Event C <t:2021-12-20T13:14:04Z>
      Event D <t:2021-12-20T12:14:04-01:00>
      Event E <t:Monday>`))

	// Output:
	// A sample set of timestamps to convert:
	//       Event A <t:1234567890:R>
	//       Event B <t:1640006044:D>
	//       Event C <t:1640006044>
	//       Event D <t:1640006044>
	//       Event E [Time TBC]
}
