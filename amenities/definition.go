package amenities

import (
	"bitbucket.org/idomdavis/etbot/colour"
	"github.com/bwmarrin/discordgo"
)

// Amenity within the tower.
type Amenity struct {
	ID      string
	Name    string
	Channel string
	Icon    string
	Colour  int
}

var defaultAmenity = Amenity{
	ID:      "default",
	Name:    "Ethereum Towers Event",
	Channel: "#lobby",
	Icon:    "https://tentcity.info/icon/et.png",
	Colour:  colour.Black,
}

// Amenities in the tower.
var Amenities = []Amenity{
	{
		ID:   "vip",
		Name: "VIP Event",
		Icon: "https://tentcity.info/icon/et.png",
	},
	{
		ID:   "global",
		Name: "Ethereum Towers",
		Icon: "https://tentcity.info/icon/et.png",
	},
	{
		ID:   "comedy",
		Name: "Comedy Club",
		Icon: "https://tentcity.info/icon/default.png",
	},
}

var amenities map[string]Amenity

// Find an Amenity by its ID.
func Find(id string) Amenity {
	lazyInit()

	amenity := defaultAmenity

	if a, ok := amenities[id]; ok {
		amenity = a
	}

	return amenity
}

// Choices available for various amenities.
func Choices() []*discordgo.ApplicationCommandOptionChoice {
	c := make([]*discordgo.ApplicationCommandOptionChoice, len(Amenities))

	for i, a := range Amenities {
		c[i] = &discordgo.ApplicationCommandOptionChoice{Name: a.Name, Value: a.ID}
	}

	return c
}

func lazyInit() {
	if amenities != nil {
		return
	}

	amenities = make(map[string]Amenity, len(Amenities))

	for _, a := range Amenities {
		amenities[a.ID] = a
	}
}
