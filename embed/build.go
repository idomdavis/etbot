package embed

import "github.com/bwmarrin/discordgo"

// Build an embed into a message.
func Build(msg string, author Author) *discordgo.MessageSend {
	embed := &discordgo.MessageEmbed{
		Description: msg,
		Color:       author.Colour,
	}

	author.Add(embed)

	return &discordgo.MessageSend{
		AllowedMentions: &discordgo.MessageAllowedMentions{
			Parse: []discordgo.AllowedMentionType{
				discordgo.AllowedMentionTypeUsers,
				discordgo.AllowedMentionTypeRoles,
			},
		},
		Embeds: []*discordgo.MessageEmbed{embed},
	}
}
