package embed

import (
	"github.com/bwmarrin/discordgo"
)

// Author of an embed.
type Author struct {
	Colour int `json:"colour"`

	Name    string `json:"name"`
	Channel string `json:"channel"`
	Icon    string `json:"icon"`
}

var unknownAuthor = Author{Name: unknownName}

const unknownName = "Someone Random"

// Add an author to the embed.
func (a Author) Add(embed *discordgo.MessageEmbed) {
	name := unknownName

	if a.Name != "" {
		name = a.Name
	}

	embed.Author = &discordgo.MessageEmbedAuthor{Name: name}

	if a.Icon != "" {
		embed.Author.IconURL = a.Icon
	}

	if a.Colour != 0 && embed.Color == 0 {
		embed.Color = a.Colour
	}
}
