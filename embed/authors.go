package embed

import (
	"bitbucket.org/idomdavis/etbot/db"
	"github.com/sirupsen/logrus"
)

// Authors known by the system.
type Authors struct {
	Connection *db.Connection
}

// Get the details for a specific author.
func (a *Authors) Get(name string) Author {
	var author Author

	if name == "" {
		return unknownAuthor
	}

	k := map[string]interface{}{"name": name}

	if err := a.Connection.Get("et-authors", k, &author); err != nil {
		logrus.WithError(err).WithField("name", name).Error("Failed to get Author")

		author = unknownAuthor
	}

	return author
}
