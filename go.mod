module bitbucket.org/idomdavis/etbot

go 1.16

require (
	bitbucket.org/idomdavis/discordance v0.3.0
	bitbucket.org/idomdavis/goconfigure v0.5.9
	github.com/aws/aws-sdk-go v1.42.4
	github.com/bwmarrin/discordgo v0.23.3-0.20211010150959-f0b7e81468f7
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20211115234514-b4de73f9ece8 // indirect
	golang.org/x/sys v0.0.0-20211116061358-0a5406a5449c // indirect
)
