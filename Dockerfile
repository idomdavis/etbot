FROM scratch

ADD etbot /
ADD avatar.png /
ADD certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["./etbot"]
