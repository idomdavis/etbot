package audit

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

// Roles in a Discord server.
type Roles []*discordgo.Role

// Sort the roles by position.
func (r Roles) Sort() {
	sort.Slice(r, func(i int, j int) bool {
		return r[i].Position < r[j].Position
	})
}

// Names of the roles in this set.
func (r Roles) Names() []string {
	names := make([]string, len(r))

	for index, role := range r {
		names[index] = role.Name
	}

	return names
}
