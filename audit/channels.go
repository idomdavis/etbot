package audit

import (
	"sort"

	"github.com/bwmarrin/discordgo"
)

// Channels in a Discord server.
type Channels []*discordgo.Channel

// Sort the channels by position.
func (c Channels) Sort() {
	sort.Slice(c, func(i int, j int) bool {
		return c[i].Position < c[j].Position
	})
}

// Names of the channels in this set.
func (c Channels) Names() []string {
	names := make([]string, len(c))

	for index, channel := range c {
		names[index] = channel.Name
	}

	return names
}
