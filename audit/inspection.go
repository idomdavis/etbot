package audit

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

// Inspection on a server.
type Inspection struct {
	Admin Roles
	Base  Roles
	Deny  Channels
	Allow Channels
	Roles map[string]int64
}

// RolePermissions we expect.
//nolint:gomnd
var RolePermissions = map[string]int64{
	"@everyone":        basePermissions,
	"Amenity Leader":   1071698538049,
	"Mods":             1440260156551,
	"Discord Security": 2196338376671,
	"Announcements":    2198083203015,
	"Admin":            2189895925759,
	"Owner":            1069551046217,
}

const basePermissions = 1069551046209

// Check the given server for permission violations.
func (i *Inspection) Check(session *discordgo.Session, id string) error {
	var (
		roles    Roles
		channels Channels
		err      error
	)

	if roles, err = session.GuildRoles(id); err != nil {
		return fmt.Errorf("failed to get roles: %w", err)
	}

	roles.Sort()

	for _, role := range roles {
		i.admin(role)
		i.base(role)
	}

	if channels, err = session.GuildChannels(id); err != nil {
		return fmt.Errorf("failed to get channels: %w", err)
	}

	channels.Sort()

	for _, channel := range channels {
		i.overwrites(channel)
	}

	return nil
}

// Check admin permissions (admin and manager) are only assigned to the Admin
// role. Roles that incorrectly have administrator permissions will be added to
// Inspection.Admin.
func (i *Inspection) admin(role *discordgo.Role) {
	const (
		allowed = "Admin"
		admin   = 1 << 3
		manager = 1 << 5
	)

	if role.Name == allowed {
		return
	}

	if role.Permissions&admin == admin || role.Permissions&manager == manager {
		i.Admin = append(i.Admin, role)
	}
}

// Base permissions check. Roles should have 0 permissions, base permissions, or
// be listed in the Inspection.Roles set with the expected permissions.
func (i *Inspection) base(role *discordgo.Role) {
	var permissions int64

	if p, ok := i.Roles[role.Name]; !ok {
		permissions = basePermissions
	} else {
		permissions = p
	}

	switch role.Permissions {
	case 0, permissions:
	// all good
	default:
		i.Base = append(i.Base, role)
	}
}

// Check the channel overwrites are as we expect.
func (i *Inspection) overwrites(channel *discordgo.Channel) {
	for _, overwrites := range channel.PermissionOverwrites {
		if overwrites.Allow != 0 {
			i.Allow = append(i.Allow, channel)
		}

		if overwrites.Deny != 0 {
			i.Deny = append(i.Deny, channel)
		}
	}
}
