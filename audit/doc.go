// Package audit provides channel and role audit functionality. The audit rules
// are:
//   * Admin role can only be held by specific roles
//   * @everyone role must match specific permissions
//   * Any roles providing extra permissions must be explicitly defined
//   * Channels should only provide visibility for a single base role
//   * Exceptions to be explicit
//   * Read Only channels can provide custom permissions for the role allowed to
//   * write.
package audit
