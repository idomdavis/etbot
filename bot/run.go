package bot

import (
	"bitbucket.org/idomdavis/discordance"
	"bitbucket.org/idomdavis/etbot/action"
	"bitbucket.org/idomdavis/etbot/aws"
	"bitbucket.org/idomdavis/etbot/config"
	"bitbucket.org/idomdavis/etbot/db"
	"bitbucket.org/idomdavis/etbot/embed"
	"github.com/sirupsen/logrus"
)

// Run the bot.
//nolint: funlen
func Run(settings config.Settings) {
	c, err := discordance.Connect(settings.Discord)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to connect to discord")
	}

	session, err := aws.Session(settings.AWS)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to get AWS session")
	}

	connection := db.New(session)
	instance, err := New(c.State.User.ID, connection)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to initialise bot startup")
	}

	authors := &embed.Authors{Connection: connection}
	announcer := action.Announcer{
		Refresh:    false,
		Bot:        instance.Name,
		Authors:    authors,
		Session:    c.Session,
		Connection: connection,
	}

	preview := action.Preview{
		Base: action.Base{
			Command:     "preview",
			Description: "Preview announcements",
			Connection:  connection,
		},
		Authors: authors,
	}

	menu := action.Menu{Base: action.Base{
		Command:     "room-service",
		Description: "Order room service",
		Connection:  connection,
	}}

	joke := action.Joke{Base: action.Base{
		Command:     "joke",
		Description: "Tell a joke",
		Connection:  connection,
	}}

	vend := action.Vend{Base: action.Base{
		Command:     "vend",
		Description: "Get an item from the vending machine",
		Connection:  connection,
	}}

	audit := action.Audit{Base: action.Base{
		Command:     "audit",
		Description: "Run a permission audit",
		Connection:  connection,
	}}

	c.Unregister(settings.Discord.Server)

	c.Commands(settings.Discord.Server,
		preview.Definition(),
		joke.Definition(),
		menu.Definition(),
		vend.Definition(),
		audit.Definition(),
	)

	c.Message(
		instance.Handler,
		joke.Handler,
		menu.Handler,
		vend.Handler,
	)

	instance.Start(c.Session)
	announcer.Run()

	c.Listen()
}
