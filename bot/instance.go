package bot

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"bitbucket.org/idomdavis/etbot/config"
	"bitbucket.org/idomdavis/etbot/db"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Instance of the bot.
type Instance struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Control string `json:"control"`

	uuid       string
	connection *db.Connection
}

// ErrBotNotDefined is returned if the bot definition cannot be found in the DB.
var ErrBotNotDefined = errors.New("bot not defined")

// New bot instance.
func New(id string, connection *db.Connection) (*Instance, error) {
	i := &Instance{
		ID:         id,
		uuid:       fmt.Sprint(time.Now().UnixNano()),
		connection: connection,
	}

	if err := i.Refresh(); err != nil {
		return i, fmt.Errorf("failed to create bot instance for %s: %w", id, err)
	}

	return i, nil
}

// Refresh the instance details from the database.
func (i *Instance) Refresh() error {
	k := map[string]interface{}{"id": i.ID}

	if err := i.connection.Get("et-bot", k, i); err != nil {
		return fmt.Errorf("failed to get bot C&C details: %w", err)
	}

	if i.Name == "" {
		return fmt.Errorf("%w: %s", ErrBotNotDefined, i.ID)
	}

	return nil
}

// Start the bot. This will attempt to shut down any other bots registered with
// the same ID and Name.
func (i *Instance) Start(session *discordgo.Session) {
	if i.Control == "" {
		return
	}

	msg := &discordgo.MessageSend{
		Content: fmt.Sprintf("%s (%s) - %s", i.Name, config.BuildVersion, i.uuid),
	}

	if _, err := session.ChannelMessageSendComplex(i.Control, msg); err != nil {
		logrus.WithError(err).Error("Failed to register with C&C")
		os.Exit(-1)
	}
}

// Handler will listen for a startup message from another bot and shut down if
// it sees one.
func (i *Instance) Handler(_ *discordgo.Session, in *discordgo.Message) {
	switch {
	case in.ChannelID != i.Control:
		return
	case in.Author.ID != i.ID:
		return
	case !strings.HasPrefix(in.Content, i.Name):
		return
	case strings.HasSuffix(in.Content, i.uuid):
		return
	}

	logrus.WithFields(logrus.Fields{
		"from":    in.Author.Username,
		"on":      in.ChannelID,
		"command": in.Content,
	}).Info("Shutting down...")

	os.Exit(0)
}
