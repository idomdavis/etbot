package config

import (
	"bitbucket.org/idomdavis/goconfigure"
)

// AWS options.
type AWS struct {
	Region    string
	Bucket    string
	AccessKey string
	SecretKey string
}

// Description for the configuration block.
func (a *AWS) Description() string {
	return "AWS Settings"
}

// Register the configuration block.
func (a *AWS) Register(opts goconfigure.OptionSet) {
	opts.Add(&goconfigure.Option{
		Pointer:     &a.Region,
		Description: "AWS Region we're connecting to for S3 storage",
		LongFlag:    "region",
		ConfigKey:   "aws-region",
		EnvVar:      "AWS_REGION",
		Default:     "eu-west-2",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.Bucket,
		Description: "AWS Bucket we're connecting to for S3 storage",
		LongFlag:    "bucket",
		ConfigKey:   "aws-bucket",
		EnvVar:      "AWS_BUCKET",
		Default:     "",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.AccessKey,
		Description: "AWS Access Key",
		ConfigKey:   "aws-access-key",
		EnvVar:      "AWS_SECRET_KEY_ID",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.SecretKey,
		Description: "AWS Secret Key",
		ConfigKey:   "aws-secret-key",
		EnvVar:      "AWS_ACCESS_KEY_ID",
	})
}

// Data for the configuration block.
func (a *AWS) Data() interface{} {
	return AWS{
		Region:    a.Region,
		Bucket:    a.Bucket,
		AccessKey: goconfigure.Sanitise(a.AccessKey, goconfigure.SET, goconfigure.UNSET),
		SecretKey: goconfigure.Sanitise(a.SecretKey, goconfigure.SET, goconfigure.UNSET),
	}
}
