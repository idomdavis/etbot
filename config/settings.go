package config

import (
	"fmt"

	"bitbucket.org/idomdavis/discordance/config"
	"bitbucket.org/idomdavis/goconfigure"
)

// Settings for the bot.
type Settings struct {
	AWS AWS

	Discord config.Discord

	Underlying goconfigure.Settings
}

const stub = "ET_BOT"

// Configure the bot and return the Settings.
func Configure() (Settings, error) {
	settings := Settings{}
	reporter := goconfigure.LogrusReporter{}

	s := goconfigure.Settings{Options: &goconfigure.Options{Stub: stub}}

	s.AddHelp()
	s.AddConfigFile()
	s.Add(&settings.AWS)
	s.Add(&settings.Discord)

	if err := s.Parse(reporter); err != nil {
		return settings, fmt.Errorf("failed to configure bot: %w", err)
	}

	settings.Underlying = s

	return settings, nil
}
