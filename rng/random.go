package rng

import (
	"crypto/rand"
	"math/big"
)

// Int returns 0 >= x < n.
func Int(n int) int {
	return int(Int64(int64(n)))
}

// Int64 returns 0 >= x < n.
func Int64(n int64) int64 {
	r, _ := rand.Int(rand.Reader, big.NewInt(n))
	return r.Int64()
}
