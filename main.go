package main

import (
	"os"

	"bitbucket.org/idomdavis/etbot/bot"
	"bitbucket.org/idomdavis/etbot/config"
	"github.com/sirupsen/logrus"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		PadLevelText:           true,
	})

	logrus.WithFields(logrus.Fields{
		"Build Version":   config.BuildVersion,
		"Build Hash":      config.BuildHash,
		"Build Timestamp": config.BuildTimestamp,
	}).Info("Build Info")

	settings, err := config.Configure()

	switch {
	case err != nil:
		logrus.WithError(err).Fatal("Failed to configure bot")
	case settings.Underlying.Help:
		settings.Underlying.Usage(os.Stdout)
		os.Exit(0)
	}

	bot.Run(settings)
}
